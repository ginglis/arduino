int forwardPin = 2;
int reversePin = 7;
int motorPin = 9;

void setup() {

pinMode(forwardPin, OUTPUT);
pinMode(reversePin, OUTPUT);
Serial.begin(9600);
}

void loop() {

// potentiometer value
int reading = analogRead(A0);

// First fifth--counter-clockwise rotation
  if (reading < 1023/2) {
     int PWM = map(reading, 0, 1023/5, 255, 0);
     digitalWrite(forwardPin, LOW);
     digitalWrite(reversePin, HIGH);
     digitalWrite(motorPin, HIGH);
     
  }
  else{
    digitalWrite(motorPint,LOW);
    }
  }
  /*/ Second fifth--no movement
  else if (reading > 1023/5 && reading < 2*1023/5) {
     int PWM = 0;
     analogWrite(motorPin, PWM);
  }
// Third fifth to full--clockwise rotation
  else if (reading > 2*1023/5) {
    int PWM = map(reading, 2*1023/5, 1023, 0, 255);
    digitalWrite(forwardPin, HIGH);
    digitalWrite(reversePin, LOW);
    analogWrite(motorPin, PWM);
  }

}
