void setup() {
  
pinMode(9,OUTPUT);   //three LEDS
pinMode(10,OUTPUT);
pinMode(11,OUTPUT);
}

void loop() {
  
int PWM=analogRead(A0)/1023.*255; // converts the reading of potentiometer to output for LEDs
int second_LED = PWM-100; // delays the voltage output to LED 2 
int third_LED = PWM-200; // delays the voltage output to LED 3
if (second_LED <= 0){ // accounts for negative values of input voltage
  second_LED=0;
  third_LED=0;
}
  else if(third_LED<=0){
  third_LED=0;
  }
  

analogWrite(9,PWM);
analogWrite(10,second_LED);
analogWrite(11,third_LED);


}
 
  
  


