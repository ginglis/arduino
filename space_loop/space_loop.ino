void setup() {
Serial.begin(9600);
char str[] = "We shall always place education side by side with instruction; the mind will not be cultivated at the expense of the heart.";
int len = strlen(str); // length of string
int i = 1; // incrementor
int index = findspace(str,i); //
int start = 0;
while (findspace(str, i) > 0){
for (int i = start; i < index; ++i){ // loop, exits before last word
Serial.print(str[i]); // prints word
}
Serial.println(); // ends the line
start = index + 1; // overpasses the space character
i++; // increase i by one
index = findspace(str, i); // finds the next space
}
for (int i = start; i < len; i++){ // no space at end, so will print last word
Serial.print(str[i]); // prints last word
}
}
void loop() {
}
int findspace(char sentence[], int x){ // function
int len=strlen(sentence); // string length
int space_counter = 0; // initialize space counter
int i; // index for for loop
int space_position = -1; // initializes space position
for (i = 0; i <= len; i++){ // will increment as many times as there are characters
if (sentence[i] == ' '){ // checks for a space
space_counter ++; // increment space counter

if (space_counter == x){ // if the space is the certain space requested by user
space_position = i; // set the space position
}
}
}
return space_position;
}

