int one_directionPin = 2;
int other_directionPin = 7;
int motorPin = 9;

void setup() {

pinMode(one_directionPin, OUTPUT);
pinMode(other_directionPin, OUTPUT);
Serial.begin(9600);
}

void loop() {

// potentiometer value
int potVal = analogRead(A0);
Serial.println(potVal);
// First fifth--counter-clockwise rotation 
if (potVal < 1023/5) {
  int pwmOut = map(potVal, 0, 1023/5, 255, 0);
  digitalWrite(one_directionPin, LOW);
  digitalWrite(other_directionPin, HIGH);
  analogWrite(motorPin, pwmOut);
  
// Second fifth--no movement
} else if (potVal > 1023/5 && potVal < 2*1023/5) {
  int pwmOut = 0;
  analogWrite(motorPin, pwmOut);

// Third fifth to full--clockwise rotation
} else if (potVal > 2*1023/5) {
  int pwmOut = map(potVal, 2*1023/5, 1023, 0, 255);
  digitalWrite(one_directionPin, HIGH);
  digitalWrite(other_directionPin, LOW);
  analogWrite(motorPin, pwmOut);

}

}
